The aim on Operation Flicker was to shut down child pornography on the internet, but in the process, they found that many government and military personnel were viewing and download child pornography on their work computers.  This operation has been kept fairly quiet, for obvious reasons.

The following is a write-up by Voice of America on the government investigation known as “Operation Flicker.”

Federal investigators have identified members of the U.S. military and defense contractors who allegedly bought and downloaded child pornography, at times on government computers.  Some of the individuals have high-level security clearances.

The Pentagon released 94 pages of documents related to investigations that date back to 2002. Many names and details are blacked out, but the facts that remain are disturbing.

A contractor with top secret clearance, tasked with providing support to the National Security Agency, was charged with possessing child pornography.  Investigators say he tampered with his work computer after agents searched his home.  He fled the country and is believed to be in Libya.  The case is closed until he is arrested and extradited.

A National Reconnaissance Office contract employee acknowledged he regularly viewed sexually explicit images of children.  

One sailor admitted to accessing child pornography while stationed on the Naval destroyer U.S.S. Mason.  He was sentenced to nearly four years in prison.

The findings come from a wider investigation conducted by Immigration and Customs Enforcement.  That probe, dubbed Operation Flicker, identified more than 5,000 individuals who subscribed to child pornography websites.   An assistant U.S. attorney general requested that the Defense Criminal Investigative Service, under the auspices of the Pentagon’s Inspector General, assist in identifying Pentagon-affiliated individuals.

